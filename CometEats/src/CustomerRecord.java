public class CustomerRecord {

    private String name;
    private String address;
    private int customerId;
    private Cart ca;
    private String password;

    public CustomerRecord(String name, String address, int customerId, Cart ca, String password) {
        this.name = name;
        this.address = address;
        this.customerId = customerId;
        this.ca = ca;
        this.password = password;
    }

    public static CustomerRecord createAndInitializeFromDb(CustomerRecord cd){
        Cart ca = Cart.initialize();
        return new CustomerRecord(cd.name, cd.address, cd.customerId, ca, cd.password);
    }

    public void addMenuItemToCart(MenuItem menuItem){
        this.ca.add(menuItem);
    }

    public Order createOrder(){
        return ca.createOrder();
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCa(Cart ca) {
        this.ca = ca;
    }

    public String getPassword() {
        return password;
    }
}
