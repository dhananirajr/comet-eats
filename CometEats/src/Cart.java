import java.util.ArrayList;

public class Cart {

    private ArrayList<MenuItem> cartItems;

    public Cart(){
        this.cartItems = new ArrayList<MenuItem>();
    }

    public static Cart initialize(){
        return new Cart();
    }

    public void add(MenuItem menuItem){
        this.cartItems.add(menuItem);
    }

    public Order createOrder(){
        return Order.create(this.cartItems);
    }

}
