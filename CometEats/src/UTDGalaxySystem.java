import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UTDGalaxySystem {

    static List<CustomerRecord> customerList;

    // initialization phase
    static {
        customerList = new ArrayList<>();
        customerList.add(new CustomerRecord("raj","plano, tx", 1, new Cart(),"1998"));
        customerList.add(new CustomerRecord("miley","richardson, tx", 2, new Cart(),"1998"));
        customerList.add(new CustomerRecord("orion","plano, tx", 3, new Cart(),"1998"));
        customerList.add(new CustomerRecord("camryn","plano, tx", 4, new Cart(),"1998"));
    }

    public static CustomerRecord authenticateUser(int userId, String password) {
        CustomerRecord c = customerList.stream().filter(customerDB -> {
            return customerDB.getCustomerId() == userId;
        }).collect(Collectors.toList()).get(0);

        if(c.getPassword().equals(password)) {
            return c;
        } else {
            return null;
        }
    }
}
