import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class GuiController {

    private CustomerRecord c;
    private ArrayList<Restaurant> lr;
    private ArrayList<MenuItem> menuItems;
    private Order order;
    private Bill bill;
    private Restaurant r;

    public static void main(String[] args) {

        GuiController guiController = new GuiController();
        Scanner in = new Scanner(System.in);
        guiController.openLoginPage();
        String userId = in.next();
        String password = in.next();
        Exception e = new Exception("Invalid credentials");

        while (e != null) { // while customer is not logged in
            try {
                guiController.provideLoginCredentials(Integer.parseInt(userId), password);
                e = null;
            } catch (Exception e_) {
                e = e_;
                System.out.println(e);
                guiController.openLoginPage();
                userId = in.next();
                password = in.next();
            }
        }


        while (true) {
            String creditCardInfo = "";

            while (true) {
                guiController.browseRestaurants(guiController.lr);
                String option = in.next();
                if (option.equalsIgnoreCase("checkout")) {
                    guiController.bill = guiController.checkout();
                    System.out.printf("your total bill is: $%.2f\n", guiController.bill.amount);
                    System.out.println("Enter your CC, CVV, and EXP:");
                    in.nextLine();
                    creditCardInfo = in.nextLine();
                    break;
                }
                guiController.selectRestaurant(Integer.parseInt(option));
                guiController.browseMenu(guiController.menuItems);

                String menuNoStr = in.next();
                int menuNo = Integer.parseInt(menuNoStr);
                MenuItem menuItem = guiController.menuItems.get(menuNo - 1);
                guiController.addMenuItemToCart(menuItem);
            }

            guiController.entersCreditCardInfo(creditCardInfo);

            System.out.println("enter \"ok\" to continue or \"logout\" to exit");
            String opt = in.nextLine();
            if (opt.equalsIgnoreCase("logout")) {
                guiController.logout();
            }
            guiController.clickOk();
            guiController.c.setCa(Cart.initialize());
        }
    }

    public GuiController() {
        this.c =null;
        this.r = new Restaurant();
    }

    public void openLoginPage() {
        System.out.println("Enter userID and password:");
    }

    public void provideLoginCredentials(int userId, String password) throws Exception {
        CustomerRecord cd = UTDGalaxySystem.authenticateUser(userId, password);
        if (cd == null) {
            throw new Exception("Invalid credentials");
        } else {
            System.out.println("logged in");
            this.c = CustomerRecord.createAndInitializeFromDb(cd);
            this.lr = (new RestaurantManager()).fetchRestaurantList();
        }
    }

    public void browseMenu(ArrayList<MenuItem> menuItems) {
        AtomicReference<Integer> count = new AtomicReference<>(1);
        menuItems.forEach(m -> {
            System.out.println(count.get() + " " + m.toString());
            count.set(count.get() + 1);
        });
        System.out.println("Select a menu item:");
    }

    public void browseRestaurants(ArrayList<Restaurant> lr) {
        AtomicReference<Integer> count = new AtomicReference<>(1);
        lr.forEach(r -> {
            System.out.println(count.get() + " " + r.getName() + " @ " + r.getAddress());
            count.set(count.get() + 1);
        });
        System.out.println("Enter the restaurant number or Enter \"checkout\" to checkout:");
    }

    public ArrayList<MenuItem> selectRestaurant(int restaurantId) {
        this.menuItems = new ArrayList<>();
        this.menuItems.addAll(this.lr.get(restaurantId - 1).getMenu());
        return this.menuItems;
    }

    public void addMenuItemToCart(MenuItem menuItem) {
        this.c.addMenuItemToCart(menuItem);
        System.out.println("Menu item added to cart.");
    }

    public Bill checkout() {
        order = this.c.createOrder();
        bill = Bill.createBill(order);
        return bill;
    }

    public void entersCreditCardInfo(String creditCardNumber) {
        PaymentManager.processPayment(creditCardNumber);
        System.out.println("order placed successfully!");
        List<Integer> x = this.order.getOrderList().stream().map(m -> m.restaurantId).collect(Collectors.toList());
        HashSet<Integer> set = new HashSet<>(x);
        set.stream().forEach(id -> {
            Restaurant r = this.lr.get(id);
            r.setCurrentWaitTime(r.getCurrentWaitTime() + 10);
            System.out.println(r.getName() + " " + r.getCurrentWaitTime() + " mins");
        });
    }

    public void clickOk() {
        System.out.println("continuing...");
    }

    public void logout() {
        this.c = null;
        System.exit(0);
    }
}
