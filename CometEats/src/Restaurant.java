import java.util.ArrayList;
import java.util.List;

public class Restaurant {

    private List<MenuItem> ml;
    private String name;
    private String address;
    private String type;
    private int currentWaitTime;

    public Restaurant(){
        this.ml = new ArrayList<MenuItem>();
        this.name = "";
        this.address = "";
        this.type = "";
        this.currentWaitTime = 0;
    }

    public Restaurant(List<MenuItem> ml, String name, String address, String type, int currentWaitTime) {
        this.ml = ml;
        this.name = name;
        this.address = address;
        this.type = type;
        this.currentWaitTime = currentWaitTime;
    }

    public List<MenuItem> getMenu(){
        return this.ml;
    }

    public int getCurrentWaitTime(){
        return this.currentWaitTime;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setCurrentWaitTime(int currentWaitTime) {
        this.currentWaitTime = currentWaitTime;
    }
}
