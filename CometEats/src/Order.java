import java.util.ArrayList;

public class Order {

    private ArrayList<MenuItem> orderList;

    public Order(){
        this.orderList = new ArrayList<MenuItem>();
    }

    public static Order create(ArrayList<MenuItem> cartItems){
        Order ord = new Order();
        ord.orderList = cartItems;
        return ord;
    }

    public ArrayList<MenuItem> getOrderList() {
        return orderList;
    }
}
