import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RestaurantManager {

    private ArrayList<Restaurant> lr;

    public ArrayList<Restaurant> fetchRestaurantList() {
        return this.lr;
    }

    public RestaurantManager() {
        ArrayList<Restaurant> restaurants = new ArrayList<>();
        restaurants.add(
                new Restaurant(Arrays.asList(
                        new MenuItem("Chicken Sandwich", 3.65, 440, 0),
                        new MenuItem("Spicy Chicken Sandwich", 4.65, 420, 0),
                        new MenuItem("Mini Chicken Sandwich", 3.85, 460, 0),
                        new MenuItem("Grilled Chicken Sandwich", 3.60, 500, 0)
                ), "Chickfila", "UTD Student Union", "Fast Food", 0)
        );
        restaurants.add(
                new Restaurant(Arrays.asList(
                        new MenuItem("Orange Chicken", 4.55, 410, 1),
                        new MenuItem("Beef and Brocolli", 2.75, 415, 1),
                        new MenuItem("Fried Rice", 8.40, 480, 1),
                        new MenuItem("Kung Pao Chicken", 3.65, 600, 1)
                ), "Panda Express", "UTD Student Union", "Fast Food", 0)
        );
        restaurants.add(
                new Restaurant(Arrays.asList(
                        new MenuItem("Whataburger", 4.55, 1000, 2),
                        new MenuItem("Whatachicken", 5.75, 1100, 2),
                        new MenuItem("Whatacatch", 4.75, 1200, 2),
                        new MenuItem("Whataburger Jr.", 3.15, 575, 2)
                ), "Whataburger", "UTD Student Union", "Fast Food", 0)
        );

        this.lr = restaurants;
    }
}
