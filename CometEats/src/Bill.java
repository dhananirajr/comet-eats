import java.util.Optional;
import java.util.function.BinaryOperator;

public class Bill {

    public double amount;

    public Bill(Order ord) {
        Optional<Double> sum = ord.getOrderList().stream().map(m -> m.getPrice()).reduce(new BinaryOperator<Double>() {
            @Override
            public Double apply(Double a, Double b) {
                return a +b;
            }
        });
        amount = sum.get();
    }

    public static Bill createBill(Order ord) {
        return new Bill(ord);
    }
}
