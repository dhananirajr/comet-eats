import java.util.ArrayList;

public class MenuItem {

    private String name;
    private double price;
    private int calories;
    public int restaurantId;

    public MenuItem(String name, double price, /*ArrayList<String> ingredients, ArrayList<String> allergens,*/ int calories, int restaurantId){
        this.name = name;
        this.price = price;
        this.calories = calories;
        this.restaurantId = restaurantId;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return  "name='" + name + '\'' +
                ", price=$" + price +
                ", calories=" + calories;
    }
}
